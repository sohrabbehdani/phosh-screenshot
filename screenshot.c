/*
 * Copyright (C) 2024 Tether Operations Limited
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Arun Mani J <arunmani@peartree.to>
 */
#include <gtk/gtk.h>
#include <glib.h>
#include <unistd.h>
#include "phosh-screenshot.h"
#include "quick-setting.h"
#include <glib/gi18n.h>

/**
 * PhoshSimpleCustomQuickSetting:
 *
 * A simple custom quick setting for demonstration purposes.
 */


struct _PhoshSimpleCustomQuickSetting {
  PhoshQuickSetting parent;

  PhoshStatusIcon *info;
};

G_DEFINE_TYPE(PhoshSimpleCustomQuickSetting, phosh_simple_custom_quick_setting, PHOSH_TYPE_QUICK_SETTING);

static int countdown = 5;
static gboolean timer_label_update(gpointer label) {
    char text[10];
    snprintf(text, sizeof(text), "%d", countdown);
    gtk_label_set_text(GTK_LABEL(label), text);
    countdown--;
    return countdown >= 0;
}

static gboolean execute_command(gpointer data) {
    system("grim -g \"$(slurp)\" - | swappy -f - -o - | pngquant -");
    return FALSE;  // Don't call this function again
}

static void on_clicked(PhoshSimpleCustomQuickSetting *self) {
    // Create a label to show the countdown
    GtkWidget *label = gtk_label_new("5");
    gtk_widget_show(label);
    gtk_container_add(GTK_CONTAINER(gtk_widget_get_parent(GTK_WIDGET(self))), label);

    // Start the countdown
    countdown = 5;
    g_timeout_add(1000, (GSourceFunc)timer_label_update, label);

    // Execute the command after 5 seconds
    g_timeout_add_seconds(5, execute_command, NULL);
}

static void phosh_simple_custom_quick_setting_class_init(PhoshSimpleCustomQuickSettingClass *klass) {
    GtkWidgetClass *widget_class = GTK_WIDGET_CLASS(klass);

    gtk_widget_class_set_template_from_resource(widget_class,
                                                "/mobi/phosh/plugins/simple-custom-quick-setting/qs.ui");

    gtk_widget_class_bind_template_child(widget_class, PhoshSimpleCustomQuickSetting, info);

    gtk_widget_class_bind_template_callback(widget_class, on_clicked);
}

static void phosh_simple_custom_quick_setting_init(PhoshSimpleCustomQuickSetting *self) {
    gtk_widget_init_template(GTK_WIDGET(self));
}

int main(int argc, char *argv[]) {
    gtk_init(&argc, &argv);

    GtkWidget *window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    PhoshSimpleCustomQuickSetting *setting = g_object_new(PHOSH_TYPE_SIMPLE_CUSTOM_QUICK_SETTING, NULL);

    gtk_container_add(GTK_CONTAINER(window), GTK_WIDGET(setting));
    gtk_widget_show_all(window);

    g_signal_connect(window, "destroy", G_CALLBACK(gtk_main_quit), NULL);
    gtk_main();

    return 0;
}

